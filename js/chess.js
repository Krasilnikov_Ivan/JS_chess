/*
В этом файле происходит инициализация игры,
обработка действий пользователя и вызов
соответствующих функций из файла
engine.js
*/


"use strict";

var game = new Game(8,8,55);

(function($){
	$(function(){
		$('body').on('mouseover','div[data-figure-name]',function(){
			figureMouseOver.call($(this));
		});
		$('body').on('mouseout','div[data-figure-name]',function(){
			figureMouseOut.call($(this));
		});
		$('body').on('click','div[data-figure-name]',function(){
			figureMouseClick.call($(this));
		});
		$('body').on('click','.highlight:not(.highlight-blue)',function(){
			highlightCellMouseClick.call($(this));
		});
	});
})(jQuery);

