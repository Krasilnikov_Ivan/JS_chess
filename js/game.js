/*
Здесь описан конструктор игры,
форитрование игрового поля
и первоначальная расстановка фигур
*/

"use strict";

class Game{
	constructor(width, height, size){
		this.setSetting(width, height, size);
		this.renderGrid();
		this.addFigures();
		this.score = [];
		this.history = [];
	}

	setSetting(width, height, size){
		this.setting = {};
		this.setting.width = width;
		this.setting.height = height;
		this.setting.size = size;
	}

	renderGrid(){
		var stage = this.createStage();
		this.grid = [];
		for(var x = 0; x < this.setting.width; x++){
			this.grid[x] = [];
			for(var y = 0; y < this.setting.height; y++){
				var node = document.createElement('div');
				node.style.width = node.style.height = this.setting.size + 'px';
				node.style.left = x * this.setting.size + 'px';
				node.style.top = y * this.setting.size + 'px';
				node.setAttribute("data-x", x);
				node.setAttribute("data-y", y);
				node.classList.add("cell");
				if (x%2){
					node.setAttribute("data-cell-color", (y%2) ? "light" : "dark"); 
				}else{
					node.setAttribute("data-cell-color", (y%2) ? "dark" : "light");
				}
				stage.appendChild(node);
				this.grid[x][y] = {
					node: node,
					val: false
				}
			}
		}
		document.body.appendChild(stage);
	}

	createStage(){
		var stage = document.createElement('div');
		stage.style.width = this.setting.width*this.setting.size+'px';
		stage.style.height = this.setting.height*this.setting.size+'px';
		stage.setAttribute("id","stage");
		document.body.appendChild(stage);
		return stage;
	}

	addFigures(){
		for(var key in FIGURES){
			for(var key2 in FIGURES[key]){
				var figure = FIGURES[key][key2];
				this.grid[figure.x][figure.y].node.setAttribute("data-figure-name", key);
				this.grid[figure.x][figure.y].node.setAttribute("data-figure-index", key2);
				this.grid[figure.x][figure.y].node.setAttribute("data-figure-color", figure.color);
				this.grid[figure.x][figure.y].node.setAttribute("data-first-move", "true");
				this.grid[figure.x][figure.y].val = true;
			}
		}
	}
}