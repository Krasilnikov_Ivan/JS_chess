/*
Здесь описаны данные о всех фигурах на шахматной доске
*/

"use strict";

var FIGURES = {
	rook: {
		0: { x: 0, y: 7, color: "dark" },
		1: { x: 7, y: 7, color: "dark" },
		2: { x: 0, y: 0, color: "light" },
		3: { x: 7, y: 0, color: "light" }
	},
	knight: {
		0: { x: 1, y: 7, color: "dark" },
		1: { x: 6, y: 7, color: "dark" },
		2: { x: 1, y: 0, color: "light" },
		3: { x: 6, y: 0, color: "light" }
	},
	bishop: {
		0: { x: 2, y: 7, color: "dark" },
		1: { x: 5, y: 7, color: "dark" },
		2: { x: 2, y: 0, color: "light" },
		3: { x: 5, y: 0, color: "light" }
	},
	king: {
		0: { x: 3, y: 7, color: "dark" },
		1: { x: 3, y: 0, color: "light" }
	},
	queen: {
		0: { x: 4, y: 7, color: "dark" },
		1: { x: 4, y: 0, color: "light" }
	},
	pawns:{
		0: { x: 0, y: 6, color: "dark" },
		1: { x: 1, y: 6, color: "dark" },
		2: { x: 2, y: 6, color: "dark" },
		3: { x: 3, y: 6, color: "dark" },
		4: { x: 4, y: 6, color: "dark" },
		5: { x: 5, y: 6, color: "dark" },
		6: { x: 6, y: 6, color: "dark" },
		7: { x: 7, y: 6, color: "dark" },
		8: { x: 0, y: 1, color: "light" },
		9: { x: 1, y: 1, color: "light" },
		10: { x: 2, y: 1, color: "light" },
		11: { x: 3, y: 1, color: "light" },
		12: { x: 4, y: 1, color: "light" },
		13: { x: 5, y: 1, color: "light" },
		14: { x: 6, y: 1, color: "light" },
		15: { x: 7, y: 1, color: "light" }
	}
};