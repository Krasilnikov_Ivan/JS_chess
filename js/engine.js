/*
Этот файл является ядром игры!
Тут описаны все функции, отвечающие 
за обработку данных, полученных 
из файла chess.js
*/

// тригер для проверки того, был ли клик по шахматной фигуре
let clickiedCell = false,
	clickedFigure;

// при наведении мыши на шахматную фигуру
function figureMouseOver(){
	let figure_name = $(this).attr("data-figure-name"),
		x = Number($(this).attr("data-x")),
		y = Number($(this).attr("data-y")),
		cell = []; // в неё будут запысиваться яцейки с возможными ходами

	if(!clickiedCell){
		if(figure_name == "pawns"){
			let figure_color = $(this).attr("data-figure-color");
			if($(this).attr("data-first-move") == "true"){
				cell.push(figure_color == "dark" ? game.grid[x][y-1] : game.grid[x][y+1]);
				cell.push(figure_color == "dark" ? game.grid[x][y-2] : game.grid[x][y+2]);
			}else{
				// если существует клетка на доске для следующего хода вперёд
				if(figure_color == "dark" ? isCellIsset(x,y-1) : isCellIsset(x,y+1)){
					// и она не занята другой фигурой
					if(!(figure_color == "dark" ? game.grid[x][y-1].val : game.grid[x][y+1].val)){
						// добавляем её в массив подствеченных фигур
						cell.push(figure_color == "dark" ? game.grid[x][y-1] : game.grid[x][y+1]);
					}
				}
				// если есть противник впереди-справа
				if(figure_color == "dark" ? isCellIsset(x+1,y-1) : isCellIsset(x+1,y+1)){
					if(figure_color == "dark" ? isFigureOpponent(game.grid[x+1][y-1].node, $(this)) : isFigureOpponent(game.grid[x+1][y+1].node, $(this))){
						cell.push(figure_color == "dark" ? game.grid[x+1][y-1] : game.grid[x+1][y+1]);
					}
				}
				// если есть противник впереди-справа
				if(figure_color == "dark" ? isCellIsset(x-1,y-1) : isCellIsset(x-1,y+1)){
					if(figure_color == "dark" ? isFigureOpponent(game.grid[x-1][y-1].node, $(this)) : isFigureOpponent(game.grid[x-1][y+1].node, $(this))){
						cell.push(figure_color == "dark" ? game.grid[x-1][y-1] : game.grid[x-1][y+1]);
					}
				}
			}
		}

		if(figure_name == "knight"){
			if(isCellIsset(x-1, y-2)){
				cell.push(game.grid[x-1][y-2]);
			}
			if(isCellIsset(x-1, y+2)){
				cell.push(game.grid[x-1][y+2]);
			}
			if(isCellIsset(x+1, y-2)){
				cell.push(game.grid[x+1][y-2]);
			}
			if(isCellIsset(x+1, y+2)){
				cell.push(game.grid[x+1][y+2]);
			}
			if(isCellIsset(x-2, y-1)){
				cell.push(game.grid[x-2][y-1]);
			}
			if(isCellIsset(x-2, y+1)){
				cell.push(game.grid[x-2][y+1]);
			}
			if(isCellIsset(x+2, y-1)){
				cell.push(game.grid[x+2][y-1]);
			}
			if(isCellIsset(x+2, y+1)){
				cell.push(game.grid[x+2][y+1]);
			}
		}

		if(figure_name == "rook"){
			// пока не придумал, как сократить эти 4 цикла в более компактный вариант
			for(let i = x + 1; i < game.setting.width; i++){
				if(game.grid[i][y].val === true){
					if(isFigureOpponent(game.grid[i][y].node, $(this))){
						cell.push(game.grid[i][y]);
					}
					break;
				}
				cell.push(game.grid[i][y]);
			}
			for(let i = y + 1; i < game.setting.width; i++){
				if(game.grid[x][i].val === true){
					if(isFigureOpponent(game.grid[x][i].node, $(this))){
						cell.push(game.grid[x][i]);
					}
					break;
				}
				cell.push(game.grid[x][i]);
			}
			for(let i = x - 1; i > -1; i--){
				if(game.grid[i][y].val === true){
					if(isFigureOpponent(game.grid[i][y].node, $(this))){
						cell.push(game.grid[i][y]);
					}
					break;
				}
				cell.push(game.grid[i][y]);
			}
			for(let i = y - 1; i > -1; i--){
				if(game.grid[x][i].val === true){
					if(isFigureOpponent(game.grid[x][i].node, $(this))){
						cell.push(game.grid[x][i]);
					}
					break;
				}
				cell.push(game.grid[x][i]);
			}
		}

		if(figure_name == "bishop"){
			// в поисках более изящного решения. Сначала напишу, чтобы работало, потом рефакторинг
			// вправо и вниз
			for(let i = x + 1, j = y + 1; i < game.setting.width; i++, j++){
				if(isCellIsset(i, j)){
					if(game.grid[i][j].val){
						if(isFigureOpponent(game.grid[i][j].node, $(this))){
							cell.push(game.grid[i][j]);
						}
						break;
					}
					cell.push(game.grid[i][j]);				
				}
			}
			// вправо и вверх
			for(let i = x + 1, j = y - 1; i < game.setting.width; i++, j--){
				if(isCellIsset(i, j)){
					if(game.grid[i][j].val){
						if(isFigureOpponent(game.grid[i][j].node, $(this))){
							cell.push(game.grid[i][j]);
						}
						break;
					}
					cell.push(game.grid[i][j]);				
				}
			}
			// влево и вверх
			for(let i = x - 1, j = y - 1; i > -1; i--, j--){
				if(isCellIsset(i, j)){
					if(game.grid[i][j].val){
						if(isFigureOpponent(game.grid[i][j].node, $(this))){
							cell.push(game.grid[i][j]);
						}
						break;
					}
					cell.push(game.grid[i][j]);				
				}
			}
			// влево и вниз
			for(let i = x - 1, j = y + 1; i > -1; i--, j++){
				if(isCellIsset(i, j)){
					if(game.grid[i][j].val){
						if(isFigureOpponent(game.grid[i][j].node, $(this))){
							cell.push(game.grid[i][j]);
						}
						break;
					}
					cell.push(game.grid[i][j]);				
				}
			}
		}

		if(figure_name == "king"){
			if(isCellIsset(x-1, y)){
				cell.push(game.grid[x-1][y]);
			}
			if(isCellIsset(x-1, y-1)){
				cell.push(game.grid[x-1][y-1]);
			}
			if(isCellIsset(x, y-1)){
				cell.push(game.grid[x][y-1]);
			}
			if(isCellIsset(x+1, y-1)){
				cell.push(game.grid[x+1][y-1]);
			}
			if(isCellIsset(x+1, y)){
				cell.push(game.grid[x+1][y]);
			}
			if(isCellIsset(x+1, y+1)){
				cell.push(game.grid[x+1][y+1]);
			}
			if(isCellIsset(x, y+1)){
				cell.push(game.grid[x][y+1]);
			}
			if(isCellIsset(x-1, y+1)){
				cell.push(game.grid[x-1][y+1]);
			}
		}

		if(figure_name == "queen"){
			// вверх
			for(let i = y - 1; i > -1; i--){
				if(game.grid[x][i].val === true){
					if(isFigureOpponent(game.grid[x][i].node, $(this))){
						cell.push(game.grid[x][i]);
					}
					break;
				}
				cell.push(game.grid[x][i]);
			}
			// вверх и влево
			for(let i = y - 1, j = x - 1; i > -1; i--, j--){
				if(isCellIsset(j, i)){
					if(game.grid[j][i].val === true){
						if(isFigureOpponent(game.grid[j][i].node, $(this))){
							cell.push(game.grid[j][i]);
						}
						break;
					}
					cell.push(game.grid[j][i]);
				}
			}
			// вверх и вправо
			for(let i = y - 1, j = x + 1; i > -1; i--, j++){
				if(isCellIsset(j, i)){
					if(game.grid[j][i].val === true){
						if(isFigureOpponent(game.grid[j][i].node, $(this))){
							cell.push(game.grid[j][i]);
						}
						break;
					}
					cell.push(game.grid[j][i]);
				}
			}
			// вправо
			for(let i = x + 1; i < game.setting.width; i++){
				if(game.grid[i][y].val === true){
					if(isFigureOpponent(game.grid[i][y].node, $(this))){
						cell.push(game.grid[i][y]);
					}
					break;
				}
				cell.push(game.grid[i][y]);
			}
			// вниз и вправо
			for(let i = y + 1, j = x + 1; i < game.setting.height; i++, j++){
				if(isCellIsset(j, i)){
					if(game.grid[j][i].val === true){
						if(isFigureOpponent(game.grid[j][i].node, $(this))){
							cell.push(game.grid[j][i]);
						}
						break;
					}
					cell.push(game.grid[j][i]);
				}
			}
			// вниз
			for(let i = y + 1; i < game.setting.height; i++){
				if(game.grid[x][i].val === true){
					if(isFigureOpponent(game.grid[x][i].node, $(this))){
						cell.push(game.grid[x][i]);
					}
					break;
				}
				cell.push(game.grid[x][i]);
			}
			// вниз и влево
			for(let i = y + 1, j = x - 1; i < game.setting.height; i++, j--){
				if(isCellIsset(j, i)){
					if(game.grid[j][i].val === true){
						if(isFigureOpponent(game.grid[j][i].node, $(this))){
							cell.push(game.grid[j][i]);
						}
						break;
					}
					cell.push(game.grid[j][i]);
				}
			}
			// влево
			for(let i = x - 1; i > -1; i--){
				if(game.grid[i][y].val === true){
					if(isFigureOpponent(game.grid[i][y].node, $(this))){
						cell.push(game.grid[i][y]);
					}
					break;
				}
				cell.push(game.grid[i][y]);
			}
		}

		cellAddHighlightThis($(this));
		cellAddHighlight(cell, $(this));
	}
}

// когда мышь покидает границы шахматной фигуры
function figureMouseOut(){
	if(!clickiedCell){
		cellRemoveHighlight();
	}
}

// при клике на шахматную фигуру
function figureMouseClick(){
	clickiedCell = true;
	clickedFigure = $(this);
}

// при клике на подсвеченную ячейку
function highlightCellMouseClick(){
	let oldCell = game.grid[clickedFigure.attr("data-x")][clickedFigure.attr("data-y")],
		newCell = game.grid[$(this).attr("data-x")][$(this).attr("data-y")];
	//console.log(oldCell.node);
	//console.log(newCell.node);
	newCell.val = true;
	newCell.node.setAttribute("data-figure-color", oldCell.node.getAttribute("data-figure-color"));
	newCell.node.setAttribute("data-figure-name", oldCell.node.getAttribute("data-figure-name"));
	newCell.node.setAttribute("data-figure-index", oldCell.node.getAttribute("data-figure-index"));
	newCell.node.setAttribute("data-first-move", "false");
	oldCell.val = false;
	oldCell.node.removeAttribute("data-figure-color");
	oldCell.node.removeAttribute("data-figure-name");
	oldCell.node.removeAttribute("data-figure-index");
	oldCell.node.removeAttribute("data-first-move");
	cellRemoveHighlightThis(oldCell.node);
	cellRemoveHighlight();
	clickiedCell = false;
}

// подсветка яцеек
function cellAddHighlight(future_cell, current_cell){
	for(let i = 0; i < future_cell.length; i++){
		// включаем подсветку
		future_cell[i].node.classList.add("highlight");
		// если ячейка уже занята другой фигурой
		if(future_cell[i].val === true){
			// если там фигура противника, то меняет подсветку на красную
			if(current_cell.attr("data-figure-color") != future_cell[i].node.getAttribute("data-figure-color")){
				future_cell[i].node.classList.add("highlight-red");
			// если же своя, то снимаем подстветку, так как нельзя ставить 2 своих фигуры в одну клетку
			}else{
				future_cell[i].node.classList.remove("highlight");
			}
		}
	}
}

function cellRemoveHighlight(){
	let highlight = document.getElementsByClassName("highlight"),
		highlight_red = document.getElementsByClassName("highlight-red"),
		highlight_blue = document.getElementsByClassName("highlight-blue");

	for(let i = 0; i < highlight.length;){
		highlight[0].classList.remove("highlight");
	}
	for(let i = 0; i < highlight_red.length.length;){
		highlight_red.length[0].classList.remove("highlight-red");
	}
	for(let i = 0; i < highlight_blue.length.length;){
		highlight_blue.length[0].classList.remove("highlight-blue");
	}
}

function cellAddHighlightThis(current_cell){
	current_cell.addClass("highlight highlight-blue");
}
function cellRemoveHighlightThis(old_cell){
	old_cell.classList.remove("highlight","highlight-blue");
}

function isFigureOpponent(future_cell, current_cell){
	let result = true;
	if((future_cell.getAttribute("data-figure-color") === null) || (future_cell.getAttribute("data-figure-color") == current_cell.attr("data-figure-color"))){
		result = false;
	}
	return result;
}

function isCellIsset(x,y){
	let result = true;
	if((x < 0) || (x >= game.setting.width) || (y < 0) || (y >= game.setting.height)){
		result = false;
	}
	return result;
}